// importScripts('../node_modules/workbox-sw/build/workbox-sw.js');
//...............................................................................................................
//declare all static files to be cached
const urlsToCache = [
    '/',
    '/css/app.css',
    '/js/app.js',
    '/category',
    '/js/pwa_checker.js',
    '/manifest.json',
    '/category/add_new_category'
];
//.............................................................................................................
//set up two constant names for caching
const set_cache_name = 'laravel_pwa_cashed_data';
// const set_online_name = 'laravel_pwa_online_data';
//.............................................................................................................
//the service worker action section
self.addEventListener('activate', function(event) {
    var cacheWhitelist = [set_cache_name];
    event.waitUntil(
        caches.keys().then(function(cacheNames) {
            return Promise.all(
                cacheNames.map(function(cacheName) {
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );
});
//.............................................................................................................
self.addEventListener('install', async event => {
    const cache = await caches.open(set_cache_name);
    cache.addAll(urlsToCache);
});
//.............................................................................................................
self.addEventListener('fetch', async event => {
    const req = event.request;
    const  url = new URL(req.url);
    //checks if there is any changes in the cache or not
    if(url.origin === location.origin){
        event.respondWith(getCacheData(req));
    }else{
        event.respondWith(fetchFromNetwork(req));
    }
});
//.............................................................................................................
async function getCacheData (req) {
    const cachedResponse = await caches.match(req);
    return cachedResponse || fetch(req);
}
//.............................................................................................................
async function fetchFromNetwork (req) {
    const cache = await caches.open(set_cache_name);
    try {
        const res = await fetch(req);
        // IMPORTANT: Clone the response. A response is a stream
        // and because we want the browser to consume the response
        // as well as the cache consuming the response, we need
        // to clone it so we have two streams.
        var responseToCache = res.clone();
        cache.put(req, responseToCache);
        return res;
    }catch (e) {
        //return not found page instead
        const cachedResponse =  await cache.match(req);
        return cachedResponse || caches.match('/fallback.json');
    }
}
//.............................................................................................................

