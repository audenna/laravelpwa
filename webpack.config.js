/**
 * As our first step, we'll pull in the user's webpack.mix.js
 * file. Based on what the user requests in that file,
 * a generic config object will be constructed for us.
 */
let mix = require('./node_modules/laravel-mix/src/index');
let SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin'); //Our magic

let ComponentFactory = require('./node_modules/laravel-mix/src/components/ComponentFactory');

new ComponentFactory().installAll();

require(Mix.paths.mix());

/**
 * Just in case the user needs to hook into this point
 * in the build process, we'll make an announcement.
 */

Mix.dispatch('init', Mix);

/**
 * Now that we know which build tasks are required by the
 * user, we can dynamically create a configuration object
 * for Webpack. And that's all there is to it. Simple!
 */

let WebpackConfig = require('./node_modules/laravel-mix/src/builder/WebpackConfig');
mix.webpackConfig({
    plugins: [
        new SWPrecacheWebpackPlugin({
            filename: 'serviceWorker.js',
            cacheId: 'pwa',
            staticFileGlobs: ['public/**/*.{css,eot,svg,ttf,woff,woff2,js,html,png,jpg}'],
            minify: true,
            stripPrefix: 'public/',
            handleFetch: true,
            ignoreUrlParametersMatching: [/login/,/register/,/^utm_/],
            dynamicUrlToDependencies: {
                //add all your files here and their relative paths
                '/': ['resources/views/welcome.blade.php'],
                '/category': ['resources/views/categories/category_view.blade.php'],
            },
            staticFileGlobsIgnorePatterns: [/\.map$/, /mix-manifest\.json$/, /manifest\.json$/, /service-worker\.js$/],
            navigateFallback: '/',
            runtimeCaching: [
                {
                    urlPattern: /^https:\/\/fonts\.googleapis\.com\//,
                    handler: 'cacheFirst'
                },
                {
                    urlPattern: /^https:\/\/www\.thecocktaildb\.com\/images\/media\/drink\/(\w+)\.jpg/,
                    handler: 'cacheFirst'
                }
            ],
            // importScripts: ['./js/push_message.js']
        })
    ]
});

module.exports = new WebpackConfig().build();
