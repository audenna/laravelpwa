<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = ['title' => 'Welcome - Laravel Progressive Web App'];


    return view('welcome', $data);
});
//...................................................................................................................
//get the route for the manifest
Route::get('/manifest.json', 'ManifestController@getMyWebManifest')->name('getMyWebManifest');
//...................................................................................................................
//the route section for Category
Route::get('/category', 'CategoryController@index')->name('categoryHome');//show category index
Route::get('/category/add_new_category', 'CategoryController@addCategory')->name('addCategory');


//...................................................................................................................
