@extends('layout')
@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Add New Category</h1>
    </div>

    <div class="container">
        <a href="{{route('categoryHome')}}">Back</a>
        <br>
        <div class="card-deck mb-3 category_container">
            <div class="card mb-4 shadow-sm">
                <form class="new_category_container" method="post" action="#">
                    <div class="row form-group">
                        <label class="col-sm-2 col-form-label">Category Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="cat_name" required placeholder="Enter category"/>
                        </div>
                    </div>
                    <div class="row form-group">
                        <label class="col-sm-2 col-form-label"></label>
                        <div class="col-sm-10">
                            <button class="btn btn-success" type="submit">Create New Category</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @include('home.footer')
    </div>
@endsection
