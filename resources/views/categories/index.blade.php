@extends('layout')
@section('content')
    <div class="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
        <h1 class="display-4">Category</h1>
        <p class="lead">This is the all category management interface</p>
    </div>

    <div class="container">
        <a href="{{route('addCategory')}}" class="btn btn-dark">Add New Category</a>
        <br>
        <div class="card-deck mb-3 text-center category_container">
            <div class="card mb-4 shadow-sm table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>S/N</th>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Date Created</th>
                        <th>Date Updated</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tfoot>
                    <tr>
                        <td>S/N</td>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Date Created</td>
                        <td>Date Updated</td>
                        <td>Status</td>
                        <td>Action</td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>
@include('home.footer')
    </div>
@endsection
