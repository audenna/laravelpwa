<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Title -->
    <title>@if($title) {{$title}} @endif | FrancoNero</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
    <link rel="shortcut icon" href="{{asset('images/icons/icon-72x72.png')}}">
    <link rel="manifest" href="{{asset('manifest.json')}}">
    <!-- Google Fonts -->
    <link href="//fonts.googleapis.com/css?family=Roboto:300,400,500" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/app.css')}}"/>

    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
        html {
            font-size: 14px;
        }
        @media (min-width: 768px) {
            html {
                font-size: 16px;
            }
        }

        .container {
            max-width: 960px;
        }

        .pricing-header {
            max-width: 700px;
        }

        .card-deck .card {
            min-width: 220px;
        }
        .new_category_container{
            padding: 20px;
        }
    </style>
</head>
<body>

@include('home.header')
    @yield('content')
<script src="{{asset('js/pwa_checker.js')}}"></script>
</body>
</html>
