<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CategoryController extends Controller{
    //display the category index file
    public function index(){
        $data = [
            'title' => 'Category Display',
            'description' => ''
        ];
//        var_dump($cat_data);
        return view('categories.index', $data);
    }
//..............................................................................................................
//show add new category page
    public function addCategory(){
        $data = [
            'title' => 'Add New category',
            'description' => ''
        ];
       return view('categories.add_category', $data);
    }
//..............................................................................................................
//show add new category page
//    public function addCategory(){
//
//    }
}
